Dim system, formats, backup_location, root_location
Set system = CreateObject("Scripting.FileSystemObject")


formats = Array(".pdf", ".doc", ".docx", ".pst", ".wab", ".xls", ".xlsx", ".ppt", ".pptx")
backup_location =   "C:\backup\"
root_location = "C:\"


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

BackupFiles(root_location)
WScript.Echo("Files copied from "& root_location & " to " & backup_location)


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Function BackupFiles(FolderName)
   On Error Resume Next
   Dim folder_path, file_path, backup_path
   Set folder_path = system.GetFolder(FolderName)
   Set file_path = folder_path.Files
   For Each file In file_path
      If ContainsFormat(Mid(file.Name, InstrRev(file.Name, "."))) Then
         backup_path = backup_location & Mid(file.Path, 4, InstrRev(file.Path, "\")- 4)
         CreateSubfolders backup_path
         system.CopyFile file.Path, backup_path & "\" 
      End If
   Next
   For Each folder In folder_path.SubFolders
      If folder.Path <> backup_location Then
         BackupFiles(folder.Path)
      End If
   Next
 '  If Err.Number = 70 Then
 '     WScript.Echo( "Access to the path '" & folder_path & "' is denied.")
 '     Err.clear
 ' End if
End Function


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Function ContainsFormat(file_format)
   ContainsFormat = False
   For Each format In formats
      If LCase(format) = LCase(file_format) Then
         ContainsFormat = True
         Exit For
      End If
   Next
End Function

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Sub CreateSubfolders(path)
   If Not system.FolderExists(system.GetParentFolderName(path)) Then
      CreateSubfolders(system.GetParentFolderName(path))
   End If
   If Not system.FolderExists(path) Then
      system.CreateFolder(path)
   End If
End Sub