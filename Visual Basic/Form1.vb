﻿' File Backup
' Author: Josef Nosov
' Version: 04/19/2013

Imports System.Threading
Imports System.IO
Public Class Backup

    'Allows user to browse through folders.
    Private folder_browser As FolderBrowserDialog = New FolderBrowserDialog()

    'Initial backup location.
    Private backup_location As String = DriveInfo.GetDrives(0).ToString + "backup"

    'Initial root location.
    Private root_location As String = DriveInfo.GetDrives(0).ToString

    'Initial list of file extentions.
    Private formats As New List(Of String)(New String() {"*.pdf", "*.doc", "*.docx", "*.pst", "*.wab", "*.xls", "*.xlsx", "*.ppt", "*.pptx"})

    Private buttons As New List(Of Button)

    'Initializes the program
    Private Sub LoadFileBackup(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        For Each format As String In formats
            FormatList.Items.Add(format.Substring(2))
        Next
        buttons.Add(Button1)
        buttons.Add(Button2)
        buttons.Add(Button3)
        buttons.Add(Button4)
        buttons.Add(Button5)
        CheckForIllegalCrossThreadCalls = False
        BackupTextBox.Text = backup_location
        RootTextBox.Text = root_location

    End Sub

    'Starts a new thread upon clicking the begin backup button.
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim T As New Thread(New ThreadStart(Sub() UpdateBackup())) : T.Start()
    End Sub


    'Recurse from the root, making copies to the backup folder upon finding files with a matching extension.
    Private Sub UpdateBackup()
        Dim backup_count As Integer = 0
        Dim count_val As Integer = 0

        For Each b As Button In buttons
            b.Enabled = False
        Next
        RadioButton1.Enabled = False
        RadioButton2.Enabled = False
        ProgressBar1.Value = 0
        Try

            Dim count_value As System.Collections.ObjectModel.ReadOnlyCollection(Of String) = My.Computer.FileSystem.GetFiles(root_location, FileIO.SearchOption.SearchAllSubDirectories, formats.ToArray)
            For Each file_located As String In count_value
                'Looks through the folder for appropriate files, if file is found then it copies the folder structure to backup.
                If RadioButton1.Checked = True And Not My.Computer.FileSystem.FileExists(backup_location + file_located.Substring(2)) Then
                    My.Computer.FileSystem.CopyFile(file_located, backup_location + file_located.Substring(2))
                    backup_count = backup_count + 1
                ElseIf RadioButton2.Checked = True And Not My.Computer.FileSystem.FileExists(backup_location + file_located.Substring(file_located.LastIndexOf("\"))) Then
                    My.Computer.FileSystem.CopyFile(file_located, backup_location + file_located.Substring(file_located.LastIndexOf("\")))
                    backup_count = backup_count + 1
                End If


                ProgressBar1.Value = (count_val / count_value.Count) * 100
                count_val = count_val + 1
                'This updates the list automatically, scrolling down with the textbox.
                ListBox1.Items.Add(file_located)
                ListBox1.TopIndex = ListBox1.Items.Count - 1
                ListBox1.Refresh()
            Next


            ProgressBar1.Value = 100
            'Upon completing, the program shows how many files have been successfully copied to the location.
            ListBox1.Items.Add("")
            ListBox1.Items.Add((backup_count).ToString + " files copied to " + backup_location)
            ListBox1.TopIndex = ListBox1.Items.Count - 1
        Catch ex As Exception
            MsgBox(Err.Description)         'If user doesnt have permission, error comes up.
        End Try

        For Each b As Button In buttons
            b.Enabled = True
        Next
        RadioButton1.Enabled = True
        RadioButton2.Enabled = True
    End Sub

    'When select backup button is clicked a folder dialog box pops up.
    Private Sub SelectBackupButton(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim folder As DialogResult = folder_browser.ShowDialog()
        If folder = Windows.Forms.DialogResult.OK Then
            backup_location = folder_browser.SelectedPath
            BackupTextBox.Text = backup_location
        End If
    End Sub

    'When select root button is clicked a folder dialog box pops up.
    Private Sub SelectRootButton(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim folder As DialogResult = folder_browser.ShowDialog()
        If folder = Windows.Forms.DialogResult.OK Then
            root_location = folder_browser.SelectedPath
            RootTextBox.Text = root_location
        End If

    End Sub

    'Only alphanumeric values can be added to this textbox, this is to prevent formatting errors.
    Private Sub AlphaNumericTextBox(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If Not (Char.IsLetterOrDigit(e.KeyChar)) Then
            If e.KeyChar = CChar(ChrW(Keys.Back)) Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        End If
    End Sub

    'Checks to see if format exists in the formats arraylist, then checks to see if its a whitespace
    'if it is neither, it accepts the new format and clears the textbox.
    Private Sub AddNewFormat(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If Not formats.Contains("*." + TextBox3.Text) And Not String.IsNullOrWhiteSpace(TextBox3.Text) Then
            formats.Add("*." + TextBox3.Text)
            FormatList.Items.Add(TextBox3.Text)
            TextBox3.Clear()
        End If
    End Sub

    'Removes format if an item is selected in the list or if it matches the value in the textbox.
    Private Sub RemoveFormat(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        If FormatList.SelectedIndex >= 0 Then
            formats.Remove("*." + FormatList.SelectedItem.ToString)
            FormatList.Items.Remove(FormatList.SelectedItem)
        ElseIf formats.Contains("*." + TextBox3.Text) Then
            formats.Remove("*." + TextBox3.Text)
            FormatList.Items.Remove(TextBox3.Text)
            TextBox3.Clear()
        End If
    End Sub

    'If textbox is selected, value from format list is deselected.
    Private Sub TextBox3_MouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles TextBox3.MouseClick
        FormatList.SelectedIndex = -1
    End Sub

    'When entering a value into text box, automatically updates.
    Private Sub RootTextBox_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles RootTextBox.KeyPress
        root_location = RootTextBox.Text
    End Sub

    'When entering a value into text box, automatically updates.
    Private Sub BackupTextBox_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles RootTextBox.KeyPress
        backup_location = BackupTextBox.Text
    End Sub

End Class
